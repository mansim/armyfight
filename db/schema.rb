# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160621063524) do

  create_table "armies", force: :cascade do |t|
    t.integer  "fight_id",      limit: 4
    t.string   "unit_name",     limit: 255
    t.integer  "hp_from",       limit: 4
    t.integer  "hp_to",         limit: 4
    t.integer  "dmg_from",      limit: 4
    t.integer  "dmg_to",        limit: 4
    t.integer  "def_inf_from",  limit: 4
    t.integer  "def_inf_to",    limit: 4
    t.integer  "def_hors_from", limit: 4
    t.integer  "def_hors_to",   limit: 4
    t.integer  "def_arch_from", limit: 4
    t.integer  "def_arch_to",   limit: 4
    t.string   "unit_type",     limit: 255
    t.integer  "columns",       limit: 4
    t.integer  "rows",          limit: 4
    t.integer  "x_pos",         limit: 4
    t.integer  "y_pos",         limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "direction",     limit: 255
    t.integer  "speed",         limit: 4
  end

  create_table "fights", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "user_id",     limit: 4
    t.string   "password",    limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "watched",     limit: 4
  end

  create_table "targets", force: :cascade do |t|
    t.integer  "army_id",    limit: 4
    t.integer  "x",          limit: 4
    t.integer  "y",          limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "stay_time",  limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "name",                   limit: 255
    t.string   "surname",                limit: 255
    t.string   "role",                   limit: 255
    t.string   "nickname",               limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

lock '3.4.0'

set :application, 'armyfight'
set :repo_url, 'git@bitbucket.org:mansim/armyfight.git'

set :passenger_restart_with_touch, true

set :deploy_to, '/home/deploy/armyfight'

set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/assets}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end